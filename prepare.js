// code to generate csv from existing json data

const fs = require("fs");
const Papa = require("papaparse");

let json_filename = "all_great_schools.json";
let csv_filename = "data.csv";

function readFile(filename) {
  console.log("reading file", filename);
  let json = JSON.parse(fs.readFileSync(filename, "utf-8"));
  return json;
}

function getData(json) {
  let output = [];

  for (let ele of json) {
    let id,
      name,
      enrollment,
      numReviews,
      studentsPerTeacher,
      ratingScale,
      rateOfLowIncome,
      rateOfAsian,
      rateOfHispanic,
      rateOfWhite,
      rateOfTwoOrMore,
      rateOfAfricanAmerican,
      rateOfAmericanNative = "";

    if (ele.id && ele.enrollment && ele.studentsPerTeacher) {
      id = ele.id;
      if (ele.name) {
        name = ele.name;
      }
      if (ele.enrollment) {
        enrollment = ele.enrollment;
      }
      if (ele.numReviews) {
        numReviews = ele.numReviews;
      }
      if (ele.studentsPerTeacher) {
        studentsPerTeacher = ele.studentsPerTeacher;
      }
      if (ele.ratingScale) {
        ratingScale = ele.ratingScale;
      }
      if (ele.ethnicityInfo) {
        for (let e of ele.ethnicityInfo) {
          if (e.label == "White" && e.percentage) {
            rateOfWhite = e.percentage;
          }
          if (e.label == "Hispanic" && e.percentage) {
            rateOfHispanic = e.percentage;
          }
          if (e.label == "Low-income" && e.percentage) {
            rateOfLowIncome = e.percentage;
          }
          if (e.label == "Asian" && e.percentage) {
            rateOfAsian = e.percentage;
          }
          if (e.label == "Two or more races" && e.percentage) {
            rateOfTwoOrMore = e.percentage;
          }
          if (e.label == "African American" && e.percentage) {
            rateOfAfricanAmerican = e.percentage;
          }
          if (e.label == "American Indian/Alaska Native" && e.percentage) {
            rateOfAmericanNative = e.percentage;
          }
        }
      }
      output.push({
        id: id,
        name: name,
        enrollment: enrollment,
        numReviews: numReviews,
        studentsPerTeacher: studentsPerTeacher,
        ratingScale: ratingScale,
        rateOfAfricanAmerican: rateOfAfricanAmerican,
        rateOfAmericanNative: rateOfAmericanNative,
        rateOfAsian: rateOfAsian,
        rateOfHispanic: rateOfHispanic,
        rateOfLowIncome: rateOfLowIncome,
        rateOfTwoOrMore: rateOfTwoOrMore,
        rateOfWhite: rateOfWhite
      });
    }
  }
  return output;
}
function convertToCSV(data) {
  let csv = Papa.unparse(data);
  fs.writeFile(csv_filename, csv, "utf-8", function() {
    console.log(csv_filename + " write successful");
  });
}

function main() {
  let json = readFile(json_filename);
  let data = getData(json);
  convertToCSV(data);
}
main();
console.log("prepare.js");
